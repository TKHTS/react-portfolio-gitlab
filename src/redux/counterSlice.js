import { createSlice } from "@reduxjs/toolkit";

export const counterSlice = createSlice({
    name: "counter",
    initialState:{
        value: 0,
        color: 5
    },
    reducers: {
        increment: (state) => {
            state.value += 1;
            state.color > 255 ? state.color = 0 : state.color += 10;
        },
        decrement: (state) => {
            state.value -= 1;
            state.color < 0 ? state.color = 254 : state.color -= 10;
        },
        incrementByAmount: (state, action) => {
            state.value += action.payload;
            state.color += action.payload;
            state.color > 255 ? state.color = 0 : state.color += action.payload;
        },
        decrementByAmount: (state, action) => {
            state.value -= action.payload;
            state.color -= action.payload;
            state.color < 0 ? state.color = 254 : state.color -= action.payload;
        }
    }
});


export const {increment, decrement, incrementByAmount, decrementByAmount } = counterSlice.actions;
export default counterSlice.reducer;