import { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import "./App.css";
import {
  increment,
  decrement,
  incrementByAmount,
  decrementByAmount,
} from "./redux/counterSlice";

function App() {
  const count = useSelector((state) => state.counter.value);
  const blue = useSelector((state) => state.counter.color);
  const dispatch = useDispatch();

  const [incrementAmount, setIncrementAmount] = useState("2");
  let color = "rgb(100, 100, " + Number(blue) + ")";
  return (
    <div className="App">
      <div className="main" style={{ backgroundColor: color }}>
        <div className="display">
          <h1>Count: {count}</h1>
        </div>
        <div className="btn-group">
          <button onClick={() => dispatch(increment())}>+ 1</button>
          <button onClick={() => dispatch(decrement())}>- 1</button>
        </div>
        <div className="btn-group">
          <input
            onChange={(e) => setIncrementAmount(e.target.value)}
            value={incrementAmount}
            placeholder="Number"
          ></input>
          </div>
          <div className="btn-group">
          <button
            onClick={() => dispatch(incrementByAmount(Number(incrementAmount)))}
          >
            Increase
          </button>
          <button
            onClick={() => dispatch(decrementByAmount(Number(incrementAmount)))}
          >
            Reduce
          </button>
        </div>
      </div>
    </div>
  );
}

export default App;
